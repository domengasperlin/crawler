# Installation instructions
1. Install Node.js (version used in this project was 10.15.1).
2. Move to folder crawler and run command ```npm install``` to install all the required packages.
3. Import the database schema provided in the file ```crawler/script.db``` and change the connectivity settings in file ```crawler/modules/database.js```.
4. Adjust crawler settings in the file ```crawler/modules/settings.js``` and connection settings in the file ```master.js``` (serving port) and ```worker.js``` (connecting address including port) or leave unchanged to use default.
5. Run master with the command ```node master seed=default```.
6. Wait for prompt "Waiting for workers..."
7. Run some workers with command ```node worker```. Workers should connect automatically to the master (with correct connection settings).

## Troubleshooting
For more detailed information and instructions, check the provided report.

## PostgreSQL warning
When importing the database without user postgres, the pgAdmin will return 12 errors for permissions. The database still gets imported, warnings can be safely ignored.
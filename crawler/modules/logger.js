const log = (message) => {
    console.log(new Date().toUTCString() + ": " + message);
};

const logWorkerCount = (message, worker, count) => {
    console.log(new Date().toUTCString() + ` [${count} workers] [${worker}]: ${message}`);
};

const logWorker = (message, worker) => {
    console.log(new Date().toUTCString() + ` [${worker}]: ${message}`);

};

module.exports = {
    Logger: {
        log,
        logWorker,
        logWorkerCount
    }
};
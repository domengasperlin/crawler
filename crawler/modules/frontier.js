/*
{
    fUrl,
    origin,
    retryCount,
    type (optional)
}
*/
const urlParser = require('url-parse');

const { settings } = require('./settings');
const { Utils } = require('./utils');

let frontier = [];
let delays = [];
let sessionQueue = new Set();

const init = async() => {
    for(let i = 0; i<settings.seedUrls.length; i++) {
        await insert({
            fUrl: settings.seedUrls[i],
            origin: null,
            retryCount: 0
        });
    }
};

const get = async() => {
    let notAllowed = [];
    let found;
    while(frontier.length > 0) {
        let item = frontier.shift();
        if(await allowed(item.fUrl)) {
            found = item;
            break;
        } else {
            notAllowed.push(item);
        }    
    }
    frontier = notAllowed.concat(frontier);
    return found;
};

const insert = async(item, forceInsert) => {
    if(forceInsert || (item && item.fUrl && !(sessionQueue.has(item.fUrl) || sessionQueue.has(await Utils.addEndingSlash(item.fUrl)) || sessionQueue.has(await Utils.trimEndingSlash(item.fUrl))))) {
        sessionQueue.add(item.fUrl);
        frontier.push(item);
    }
};

const insertDelay = async(domain, delay) => {
    delays.push({
        domain, 
        allowed: new Date().getTime()/1000 + delay
    });
}

const allowed = async(url) => {
    let {protocol, host} = urlParser(url);
    let allowed = true;
    
    delays = delays.filter(delay => delay.allowed > new Date().getTime()/1000);
    delays.forEach(delay => {
        if(delay.domain == `${protocol}//${host}`) {
            allowed = false;
        }
    });
    return allowed;
};

const insertAtStart = async(item) => {
    frontier.unshift(item);
};

const count = async() => {
    return frontier.length;
}

module.exports = {
    Frontier: {
        init,
        get,
        insert,
        insertAtStart,
        insertDelay,
        count
    }
}
const { Logger } = require('./logger');

let settings = {
    seedUrls: [],
    permittedUrls: [
        /.*\.gov\.si.*/
    ],
    exlusions: [
        '\"\"',
        '/'
    ],
    maxRetryCount: 5,
    maxFileSize: 10000000,
    fileExtensions: [
        'pdf',
        'doc', 
        'docx',
        'ppt', 
        'pptx',
        'xls',
        'xlsx',
        'pps',
        'ppsx',
        'ppsm'
    ],
    blackListedExtensions: [
        'zip',
        'mp4',
        'csv',
        'swf',
        'mp3',
        'm4a'
    ],
    imageExtensions: [
        'jpg',
        'jpeg',
        'png',
        'gif',
        'ps',
        'tif'
    ],
    disableDownloads: false,
    maxUrlLength: 5000
};
let defaultSeedUrls = [
    'https://e-uprava.gov.si',
    'https://evem.gov.si',
    'https://podatki.gov.si',
    'http://www.e-prostor.gov.si',
    'http://www.fu.gov.si',
    'http://www.gov.si',
    'http://www.ukom.gov.si',
    'http://www.mizs.gov.si',
    'http://www.ursjv.gov.si'
];

const init = async () => {
    process.argv.forEach(arg => {
        let split = arg.split("=");
        if(split[0] === 'seed') {
            if(split.length === 1) {
                settings.seedUrls = defaultSeedUrls;
            } else if (split.length === 2) {
                if(split[1] === 'default') {
                    settings.seedUrls = defaultSeedUrls;
                } else {
                    settings.seedUrls = split[1].split(',');
                }
            }
        }
    });

    Logger.log(`Setting seed urls to ${settings.seedUrls.join(', ')}.`);
    Logger.log(`Setting permitted urls to ${settings.permittedUrls.join(', ')}.`);
};

const isCrawlingAllowed = async(url) => {
    if(settings.permittedUrls.length > 0) {
        let found = false;
        settings.permittedUrls.forEach(permitted => {
            if(url.match(permitted)) {
                found = true;
            }
        });
        return found && !(await isFileBlacklisted(url));
    }
    return !(await isFileBlacklisted(url));
};

const isFileBlacklisted = async(url) => {
    let found = false;
    settings.blackListedExtensions.forEach(extension => {
        if(url.match(new RegExp(`.*\\.${extension}`, 'gi'))) {
            found = true;
        }
    });
    return found;
};

module.exports = {
    Settings: {
        init,
        isCrawlingAllowed
    },
    settings
}
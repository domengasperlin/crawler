const puppetter = require('puppeteer');
const fetch = require('node-fetch');

let browser;
let page;

const init = async () => {
    browser = await puppetter.launch({args: ['--no-sandbox', '--disable-setuid-sandbox'], ignoreHTTPSErrors: true});
    page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 1080 });
    await page.setRequestInterception(true);
    page.on('request', (req) => {
        if(req.resourceType() == 'stylesheet' || req.resourceType() == 'font' || req.resourceType() == 'image'){
            req.abort();
        }
        else {
            req.continue();
        }
    });
}

const download = async (url) => {
    let response;
    page.on('response', res => {
        if(!response) {
            response = res;
        }
    });
    const content = await page.goto(url, {
        timeout: 10000,
        waitUntil: "networkidle2"
    });
    while(!response) {}
    return {
        page,
        content,
        response
    };
}

const downloadRobots = async (protocol, host) => {
    try {
        let {page, content} = await download(`${protocol}//${host}/robots.txt`);
        let url = await content.url();
        if(content._status == 200 && url.includes('robots.txt')) {
            let text = await content.text();
            return text;
        }
        return false;
    } catch (err) {
        return false;
    }    
}

const downloadSitemap = async (sitemap) => {
    try {
        let {page, content} = await download(sitemap);
        if(content._status == 200) {
            let text = await content.text();
            return text;
        }
        return false;
    } catch (err) {
        return false;
    }
};

const downloadPage = async (url) => {
    try {
        let {page, content, response} = await download(url);
        let text = await content.text();
        let url2 = await content.url();
        return {
            text,
            url: url2,
            response: response,
            status: content._status
        };
    } catch (err) {
        return {
            test: null,
            url: null,
            response: null,
            status: 500 || content._status
        };
    }
};

const downloadBinaryFile = async(url, maxFileSize) => {
    try {
        let res = await fetch(url, {size: maxFileSize});
        return {
            binary: Buffer.from(await res.buffer()),
            contentType: await res.headers.get('content-type')
        };
    } catch (err) {
        return {
            binary: null,
            contentType: null
        };
    }   
};


module.exports = {
    Downloader:  {
        init,
        download,
        downloadRobots,
        downloadSitemap,
        downloadPage,
        downloadBinaryFile
    }
};
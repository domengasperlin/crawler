const urlParser = require('url-parse');

let settings;

const init = async(set) => {
    settings = set;
};

const urlsEqual = async(url1, url2) => {
    if(url1.slice(-1) == '/') {
        url1 = url1.substring(0, url1.length - 1);
    }
    if(url2.slice(-1) == '/') {
        url2 = url2.substring(0, url2.length - 1);
    }
    return url1 == url2;
};

const trimEndingSlash = async(url) => {
    if(url.slice(-1) == '/') {
        url = url.substring(0, url.length - 1);
    }
    return url;
};

const addEndingSlash = async(url) => {
    if(url.slice(-1) != '/') {
        url += '/';
    }
    return url;
};

const checkIfFile = async(url) => {
    let found = false;
    settings.fileExtensions.forEach(extension => {
        if(url.match(new RegExp(`.*\\.${extension}`, 'gi'))) {
            found = true;
        }
    });
    return found;
};

const checkIfImage = async(url) => {
    let found = false;
    settings.imageExtensions.forEach(extension => {
        if(url.match(new RegExp(`.*\\.${extension}`, 'gi'))) {
            found = true;
        }
    });
    return found;
};

const getImageDetails = async(url) => {
    let ext;
    let filename;
    settings.imageExtensions.forEach(extension => {
        let match = new RegExp(`.*\\/(.*)\\.${extension}`, 'gi').exec(url);
        if(match) {
            filename = match[1];
            ext = extension;
        }
    });
    return {
        ext,
        filename
    };
};

const getFileExtension = async(url) => {
    let ext;
    settings.fileExtensions.forEach(extension => {
        if(url.match(new RegExp(`.*\\.${extension}`, 'gi'))) {
            ext = extension;
        }
    });
    return ext;
};

const checkOnclick = async(text) => {
    let regex1 = /onclick=\"location.href=(.*?)\"/gi;
    let regex2 = /onclick=\"document.location=(.*?)\"/gi;
    let match;
    let matches = [];

    do {
        match = regex1.exec(text);
        if (match && match.length > 1) {
            matches.push(match[1].slice(1, -1));
        }
    } while (match);

    do {
        match = regex2.exec(text);
        if (match && match.length > 1) {
            matches.push(match[1].slice(1, -1));
        }
    } while (match);   
    return matches;
}

const processUrls = async(fUrl, baseHref, array, type) => {
    if(!baseHref) {
        baseHref = fUrl;
    }
    let frontier = [];
    let links = [];
    array.forEach(url => {
        let {host, pathname, protocol} = urlParser(url);
        if(!host) {
            if(pathname && !settings.exlusions.includes(pathname) && !protocol.includes('javascript:') && !pathname.includes('javascript:') && !url.includes(';base64,')) {
                if(baseHref.slice(-1) != '/' && pathname[0] != '/') {
                    pathname = '/' + pathname;
                }
                frontier.push({
                    fUrl: baseHref + pathname,
                    origin: fUrl,
                    retryCount: 0,
                    type
                });
                links.push({
                    from: fUrl,
                    to: baseHref + pathname,
                    redirect: false
                });
            }
        } else {
            frontier.push({
                fUrl: protocol + "//" + host + pathname,
                origin: fUrl,
                retryCount: 0,
                type
            });
            links.push({
                from: fUrl,
                to: protocol + "//" + host + pathname,
                redirect: false
            });
        }
    });
    return {
        frontier,
        links
    };
};

module.exports = {
    Utils: {
        init,
        urlsEqual,
        trimEndingSlash,
        addEndingSlash,
        checkIfFile,
        getFileExtension,
        checkIfImage,
        getImageDetails,
        checkOnclick,
        processUrls
    }
}
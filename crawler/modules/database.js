// docker run -d --name crawler-db -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=crawling -p 5432:5432 postgres:latest

const { Pool } = require('pg');
const { Utils } = require('./utils');
const { Logger } = require('./logger');
const { Settings } = require('./settings'); 


const pool = new Pool({
    user: process.env.POSTGRES_USER || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    database: process.env.POSTGRES_DB || 'crawling',
    password: process.env.POSTGRES_PASSWORD || 'postgres',
    port: process.env.POSTGRES_PORT ||  5432,
    max: 50
});

Logger.log(`Connectiong to ${JSON.stringify(pool)}.`);

pool.connect();

const getDomain = async(domain) => {
    let query = await pool.query(`SELECT * FROM crawldb.site WHERE domain = $1`, [domain]);
    if(query.rows.length == 1) {
        return query.rows[0];
    }
    return false;
}

const insertDomain = async(domain, robots, sitemap) => {
    try {
        return await pool.query(`INSERT INTO crawldb.site (domain, robots_content, sitemap_content) VALUES ($1, $2, $3)`, [domain, robots, sitemap])
    } catch (e) {
        return null;
    }
};

const getPage = async(pageUrl) => {
    try {
        let dbPage = await pool.query(`SELECT * FROM crawldb.page WHERE url = $1 or url = $2 or url = $3`, [pageUrl, await Utils.addEndingSlash(pageUrl), await Utils.trimEndingSlash(pageUrl)]);
        if(dbPage.rows.length == 1) {
            let page = dbPage.rows[0];
            if(page.page_type_code == 'REDIRECT') {
                let linkedPage = await getLinksTo(page.id);
                return await getPageById(linkedPage.to_page);
            } else if(page.page_type_code == 'FRONTIER') {
                return false;
            } else {
                return page;
            }
        } else {
            return false;
        }
    } catch (e) {
        return false;
    } 
}

const getPageById = async(pageId) => {
        let query = await pool.query("SELECT * FROM crawldb.page WHERE id = $1", [pageId]);
        if(query.rowCount == 1) {
            return query.rows[0];
        }
        return false;
};

const insertPage = async(siteId, pageTypeCode, url, htmlContent, httpStatusCode, hash, duplicate_to) => {
    try {
        let page = await pool.query(`SELECT * FROM crawldb.page WHERE url = $1 or url = $2 or url = $3`, [url, await Utils.addEndingSlash(url), await Utils.trimEndingSlash(url)]);
        if(page.rowCount != 0) url = page.rows[0].url;
        return await pool.query(`INSERT INTO crawldb.page (site_id, page_type_code, url, html_content, http_status_code, accessed_time, hash, duplicate_to) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        ON CONFLICT (url) DO UPDATE SET site_id = $1, page_type_code = $2, html_content = $4, http_status_code = $5, accessed_time = $6, hash = $7, duplicate_to = $8`, [siteId, pageTypeCode, url, htmlContent, httpStatusCode, new Date(), hash, duplicate_to]);
    } catch (e) {
        return null;
    }
};

const getLinksTo = async(from) => {
    let query = await pool.query("SELECT * FROM crawldb.link WHERE from_page = $1 and redirect = $2", [from, true]);
    if(query.rowCount == 1) {
        return query.rows[0];
    }
    return false;
}

const insertLink = async(from, to, redirect) => {
    try {
        let fromId = await getPageId(from);
        if(!fromId) {
            if(await Settings.isCrawlingAllowed(from)) {
                await insertPage(null, 'FRONTIER', from, null, null, null);
            } else {
                await insertPage(null, 'BLACKLIST', from, null, null, null);
            }
            fromId = await getPageId(from);
        }
        let toId = await getPageId(to);
        if(!toId) {
            if(await Settings.isCrawlingAllowed(to)) {
                await insertPage(null, 'FRONTIER', to, null, null, null);
            } else {
                await insertPage(null, 'BLACKLIST', to, null, null, null);
            }
            toId = await getPageId(to);
        }
        return await pool.query(`INSERT INTO crawldb.link (from_page, to_page, redirect) VALUES ($1, $2, $3)`, [fromId, toId, redirect])
    } catch (e) {
        return null;
    }
}

const pageExists = async(url) => {
    let query = await pool.query("SELECT * FROM crawldb.page WHERE url = $1 or url = $2 or url = $3", [url, await Utils.addEndingSlash(url), await Utils.trimEndingSlash(url)]);
    if(query.rows.length == 1) {
        return query.rows[0];
    }
    return false;
}

const linkExists = async(from, to) => {
    let pageId1 = await getPageId(from);
    let pageId2 = await getPageId(to);
    if(pageId1 && pageId2) {
        let query = await pool.query("SELECT * FROM crawldb.link WHERE (from_page = $1 and to_page = $2) or (from_page = $2 and to_page = $1)", [pageId1, pageId2]);
        if(query.rows.length == 1) {
            return query.rows[0];
        }
    }
    return false;
}

const getPageId = async(url) => {
    let query = await pool.query("SELECT * FROM crawldb.page WHERE url = $1 or url = $2 or url = $3", [url, await Utils.addEndingSlash(url), await Utils.trimEndingSlash(url)]);
    if(query.rows.length == 1) {
        return query.rows[0].id;
    }
    return false;
}

const insertFile = async(page_id, data_type_code, data) => {
    try {
        return await pool.query(`INSERT INTO crawldb.page_data (page_id, data_type_code, data) VALUES ($1, $2, $3)`, [page_id, data_type_code, data]);
    } catch (e) {
        return null;
    }
};

const fileExists = async(page_id) => {
    let query = await pool.query(`SELECT * FROM crawldb.datapage_data WHERE page_id = $1`, [page_id]);
    if(query.rowCount == 1) {
        return query.rows[0];
    }
    return false;
};

const insertImage = async(page_id, filename, content_type, data) => {
    try {
        return await pool.query(`INSERT INTO crawldb.image (page_id, filename, content_type, data, accessed_time) VALUES ($1, $2, $3, $4, $5)`, [page_id, filename, content_type, data, new Date()]);
    } catch (e) {
        return null;
    }
}

const hashExists = async(hash) => {
    let query = await pool.query(`SELECT * FROM crawldb.page WHERE hash = $1`, [hash]);
    if(query.rowCount == 1) {
        return query.rows[0];
    } else {
        return false;
    }
};

module.exports = {
    ConnectionPool: pool,
    Database: {
        getDomain,
        insertDomain,
        getPage,
        insertPage,
        insertLink,
        pageExists,
        getPageId,
        linkExists,
        insertFile,
        fileExists,
        insertImage,
        hashExists
    }
};

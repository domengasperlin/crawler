git fetch;

if [ $(git rev-parse HEAD) != $(git rev-parse @{u}) ]; then
	# rm -r /root/.pm2/logs/*;
	#mkdir /root/.pm2/logs	
	git pull;
	sleep 5;
	pm2 delete spawn_workers.json;
	sleep 5;
	pm2 start spawn_workers.json;
else
	echo "Up to date"
fi

const { Settings, settings } = require('./modules/settings');
const { Frontier } = require('./modules/frontier');
const { Logger } = require('./modules/logger');
const { Database } = require('./modules/database');

const socket = require('socket.io');
const urlParser = require('url-parse');

const io = socket.listen(3230);

const start = async() => {
    require('dotenv').config();
    await Settings.init();
    await Frontier.init();
    Logger.log("Waiting for workers...");
    
    let workers = [];
    let domains = [];

    io.on('connection', function (socket) {
        //connection
        Logger.log('New worker connected: ' + socket.client.id);
        workers.push({
            id: socket.client.id,
            job: null    
        });
    
        socket.on('disconnect', async() => {
            let worker = workers.find(w => w.id == socket.client.id);
            if(worker) {
                if(worker.job) {
                    worker.job.retryCount++;
                    await Frontier.insertAtStart(worker.job);
                    Logger.log(`Reinserting ${worker.job.fUrl} to frontier.`);
                }
                workers = workers.filter(worker => worker.id != socket.client.id);
            }
            Logger.log("Worker disconnected: " + socket.client.id);
        });
    
        //respond
        socket.on('requestSettings', async(settingsFn) => {
            settingsFn(settings);
        });

        socket.on('requestJob', async(jobFn) => {
            let worker = workers.find(w => w.id == socket.client.id);
            if(await Frontier.count() == 0) {
                if(worker) worker.job = null;
                jobFn();
                return;
            }
            let item = await Frontier.get();
            if(item) {
                var { fUrl, origin, retryCount, type } = item; 
            } else {
                if(worker) worker.job = null;
                jobFn();
                return;
            }
            Logger.logWorkerCount(`Processing url: ${fUrl} (remaining : ${await Frontier.count()}).`, socket.client.id, workers.length);       

            let { protocol, host, pathname, port, query } = urlParser(fUrl);
            let domain = await Database.getDomain(host);
            if(!domain) {
                let q = domains.find(domain => domain.host == host);
                if(q) {
                    q.queue.push({ 
                        fUrl, 
                        origin, 
                        retryCount, 
                        type 
                    });
                    if(worker) worker.job = null;
                    jobFn();
                    return;
                }  else {
                    domains.push({
                        host,
                        queue: []
                    });
                }
            }
            let page = await Database.getPage(fUrl);
  
            if(worker) {
                worker.job = {
                    fUrl, 
                    origin, 
                    retryCount, 
                    type
                };
            } else {
                await Frontier.insertAtStart({ 
                    fUrl, 
                    origin, 
                    retryCount, 
                    type 
                });
                if(worker) worker.job = null;
                jobFn();
                return;
            }          

            jobFn({
                type,
                origin,
                retryCount,
                url: {
                    fUrl,
                    protocol,
                    host,
                    pathname,
                    port,
                    query
                },
                domain,
                page
            });
        });

        socket.on('frontierDelay', async(obj) => {
            await Frontier.insertDelay(obj.url, obj.delay);
        });

        socket.on('frontierInsert', async(array) => {
            for(let i = 0; i<array.length; i++) {
                if(await Settings.isCrawlingAllowed(array[i].fUrl)) {
                    if(array[i].forceInsert) {
                        await Frontier.insert(array[i], true);       
                    } else {
                        await Frontier.insert(array[i]);       
                    }
                }
            }
        });

        socket.on('frontierInsertStart', async(obj) => {
            await Frontier.insertAtStart(obj);
        });

        socket.on('newDomain', async(obj) => {
            await Database.insertDomain(obj.url, obj.robots || null, obj.sitemapString || null);
            let domain = await Database.getDomain(obj.url);
            for(let i = 0; i<obj.downloaded.urls.length; i++) {
                await Database.insertPage(domain.id, 'SITEMAP', obj.downloaded.urls[i], obj.downloaded.sitemaps[i], 200);
            }
            let d = domains.find(domain => domain.host == obj.url);
            for(let i = 0; i<d.queue.length; i++) {
                await Frontier.insertAtStart(d.queue[i]);
            }
            domains = domains.filter(domain => domain.host != obj.url);

        });

        socket.on('newImage', async(obj) => {
            let domain = await getDomain(obj.fUrl);
            await Database.insertPage(domain.id, 'BINARY', obj.fUrl, null, null);
            await Database.insertImage(await Database.getPageId(obj.fUrl), obj.filename, obj.contentType, obj.binary);
        });

        socket.on('newFile', async(obj) => {
            let domain = await getDomain(obj.fUrl);
            await Database.insertPage(domain.id, 'BINARY', obj.fUrl, null, null);
            await Database.insertFile(await Database.getPageId(obj.fUrl), obj.extension.toUpperCase(), obj.binary);
        });

        socket.on('newPages', async(arr, callback) => {
            for(let i = 0; i<arr.length; i++) {
                let page = arr[i];
                let domain = await getDomain(page.fUrl);
                let page2 = await Database.hashExists(page.hash);
                if(page2 && page2.url != page.url) {
                    await Database.insertPage(domain.id, 'DUPLICATE', page.url, null, page.status, page.hash, page2.id);
                } else {
                    await Database.insertPage(domain.id, page.what, page.url, page.text, page.status, page.hash, null);
                }
            }
            if(callback) {
                callback();
            }
        });

        socket.on('newLinks', async(arr) => {
            for(let i = 0; i<arr.length; i++) {
                let link = arr[i];
                await Database.insertLink(link.from, link.to, link.redirect);
            }
        });
    });

    const getDomain = async(url) => {
        let url2 = urlParser(url);
        return await Database.getDomain(url2.host);
    };
};

start();

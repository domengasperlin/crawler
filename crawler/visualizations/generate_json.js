const { Pool } = require('pg');

const pool = new Pool({
    user: process.env.POSTGRES_USER || 'postgres',
    host: process.env.DB_HOST || 'localhost',
    database: process.env.POSTGRES_DB || 'crawling',
    password: process.env.POSTGRES_PASSWORD || 'postgres',
    port: process.env.POSTGRES_PORT ||  5432,
    max: 50
});

pool.connect();

let json = {
    nodes: [],
    links: []
};

const run = async() => {
    let domains = (await pool.query("select domain from crawldb.site")).rows;
    domains = domains.map(domain => {
        json.nodes.push({name: domain.domain});
        return domain.domain;
    });
    let query = await pool.query('select c1.domain t1, c2.domain t2 from "crawldb".site c1, "crawldb".site c2 WHERE exists (select * from "crawldb".link where from_page = c1.id and to_page = c2.id);');
    query.rows.forEach(item => {
        let index1 = domains.indexOf(item.t1);
        let index2 = domains.indexOf(item.t2);
        json.links.push({
            source: index1,
            target: index2,
            weight: 1
        });
    });
    const fs = require('fs');
    fs.writeFile("generated.json", JSON.stringify(json), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
        process.exit(0);
    }); 
};

run();

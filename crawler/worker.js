const { Downloader } = require('./modules/downloader');
const { Logger } = require('./modules/logger'); 
const { Utils } = require('./modules/utils');

const io = require('socket.io-client');
const robotsParser = require('robots-parser');
const sm = require('sitemapper');
const urlParser = require('url-parse');
const {parse} = require('node-html-parser');
const crypto = require('crypto');

const siteMapper = new sm();
siteMapper.timeout = 5000;
const socket = io.connect("http://localhost:3230/", {
    reconnection: true
});
let settings;

const run = async() => {
    socket.on('connect', async() => {
        await initWorker();
    });
    
    const initWorker = async() => {
        socket.emit('requestSettings', async(set) => {
            settings = set;
            require('dotenv').config();
            require('events').EventEmitter.prototype._maxListeners = 100;
            await Utils.init(settings);
            await Downloader.init();
            while(true) {
                await workerLoop();    
            }
        });
    };
    
    const workerLoop = async() => {
        return new Promise(resolve => {
            socket.emit('requestJob', async(job) => {
                if(!job) {
                    resolve();
                    return;
                }
                let { type, origin, retryCount, url, domain, page } = job;
                Logger.logWorker(`Received job: ${url.fUrl}`, socket.id);
                let robots;
                let sitemaps = [];
                let newDomain = false;
                let frontierQueue = [];
                let pageQueue = [];
                let linkQueue = [];

                if(!domain) {
                    Logger.logWorker('First time processing this domain.', socket.id);
                    newDomain = true;
                    domain = {
                        domain: url.host,
                        robots_content: await Downloader.downloadRobots(url.protocol, url.host)
                    };
                }

                if(domain.robots_content) {
                    try {
                        robots = robotsParser(url.protocol + '//' + url.host + '/robots.txt', domain.robots_content);
                        sitemaps = robots.getSitemaps();
                    } catch (e) {
                        Logger.logWorker("Failed getting robots.", socket.id);
                    }
                }   
            
                if(robots) {
                    let delay = robots.getCrawlDelay();
                    if(delay) {
                        socket.emit('frontierDelay', {url: `${url.protocol}//${url.host}`, delay});
                    }
                }

                if(newDomain) {
                    let urls = [];
                    let downloaded = [];
                    for(let i = 0; i<sitemaps.length; i++) {
                        let d = await Downloader.downloadSitemap(sitemaps[i]);
                        if(downloaded) {
                            urls.push(sitemaps[i]);
                            downloaded.push(d);
                        }
                    }
                    
                    socket.emit('newDomain', {
                        url: url.host,
                        robots: domain.robots_content,
                        sitemapString: downloaded.join('\n'),
                        downloaded: {
                            urls,
                            sitemaps: downloaded
                        }
                    });     
                }

                if(sitemaps.length > 0) {
                    for(let i = 0; i<sitemaps.length; i++) {
                        let s = await siteMapper.fetch(sitemaps[i]);
                        s.sites.forEach(site => {
                            frontierQueue.push({
                                fUrl: site,
                                origin: sitemaps[i],
                                retryCount: 0
                            });
                        });
                    }
                }

                if(type && type == 'image' && !page) { //direct image (with img tag)
                    if(settings.disableDownloads) {
                        Logger.logWorker(`Downloading images disabled.`, socket.id);
                        resolve();
                        return;
                    }
                    let {ext, filename} = await Utils.getImageDetails(url.fUrl);
                    let {binary, contentType} = await Downloader.downloadBinaryFile(url.fUrl, settings.maxFileSize);
                    if(!ext || !filename) {
                        let contentTypeSplit = contentType.split('/');
                        ext = contentTypeSplit[1];
                        let fUrlSplit = url.fUrl.split('/');
                        filename = fUrlSplit[fUrlSplit.length - 1];
                    }
                    socket.emit('newImage', {
                        fUrl: url.fUrl,
                        filename: `${filename}.${ext}`,
                        contentType,
                        binary
                    });
                } else if(await Utils.checkIfFile(url.fUrl) && !page) { //permitted file to download
                    if(settings.disableDownloads) {
                        Logger.logWorker(`Downloading files disabled.`, socket.id);
                        resolve();
                        return;
                    }
                    let ext = await Utils.getFileExtension(url.fUrl);
                    let {binary} = await Downloader.downloadBinaryFile(url.fUrl, settings.maxFileSize);
                    socket.emit('newFile', {
                        fUrl: url.fUrl,
                        extension: ext,
                        binary
                    });
                } else if(await Utils.checkIfImage(url.fUrl) && !page) { //linked image (with href)
                    if(settings.disableDownloads) {
                        Logger.logWorker(`Downloading images disabled.`, socket.id);
                        resolve();
                        return;
                    }
                    let {ext, filename} = await Utils.getImageDetails(url.fUrl);
                    let {binary, contentType} = await Downloader.downloadBinaryFile(url.fUrl, settings.maxFileSize);
                    socket.emit('newImage', {
                        fUrl: url.fUrl,
                        filename: `${filename}.${ext}`,
                        contentType,
                        binary
                    });
                } else if(!domain.robots_content || robots.isAllowed(url.protocol + '//' + url.host + url.pathname)) { //everything else
                    let {text, downloaded, status, url2, response} = await getPage(url.fUrl, page);

                    if(!page && response && response._headers && response._headers['content-type'] ){
                        let contentType = response._headers['content-type'];
                        if(contentType.includes('image/') && !settings.disableDownloads) {
                            let extensions = contentType.split('/');
                            let split = url.fUrl.split('/');
                            socket.emit('newImage', {
                                fUrl: url.fUrl,
                                filename: `${split[split.length -1]}.${extensions[1]}`,
                                contentType,
                                text
                            });
                            resolve();
                            return;
                        }
                    }

                    let parsedUrl = urlParser(url2);
                    let parsed = parse(text);
            
                    let baseHref = parsed.querySelectorAll("base");
                    if(baseHref.length == 1) {
                        baseHref = baseHref[0].attributes.href;
                    } else if(url2) {
                        baseHref = `${parsedUrl.protocol}//${parsedUrl.host}`;
                    } else {
                        if(retryCount == settings.maxRetryCount) {
                            Logger.logWorker(`Max retry count reached. Inserting to database.`, socket.id);
                            socket.emit('newPages', [{
                                fUrl: url.fUrl,
                                url: url.fUrl,
                                text,
                                what: 'HTML',
                                status
                            }]);
                        } else {
                            let c = retryCount + 1;
                            Logger.logWorker(`No content while getting data. Reinserting to queue... (attempt #${c})`, socket.id);
                            socket.emit('frontierInsert', [{
                                fUrl: url.fUrl,
                                origin,
                                retryCount: c,
                                forceInsert: true
                            }]);
                        }
                        resolve();
                        return;
                    }            
            
                    let hrefArray = [];
                    parsed.querySelectorAll("a").forEach(raw => {
                        if(raw && raw.attributes && raw.attributes.href && raw.attributes.href.length < settings.maxUrlLength) {
                            hrefArray.push(raw.attributes.href);
                        }
                    });
            
                    let imgArray = [];
                    if(!settings.disableDownloads) {
                        parsed.querySelectorAll("img").forEach(raw => {
                            if(raw && raw.attributes && raw.attributes.src && raw.attributes.src.length < settings.maxUrlLength) {
                                imgArray.push(raw.attributes.src);
                            }
                        });
                    }

                    let {frontier, links} = await Utils.processUrls(url.fUrl, baseHref, hrefArray.concat(imgArray).concat( await Utils.checkOnclick(text)));
                    frontierQueue = frontierQueue.concat(frontier);
                    
                    if(downloaded && !page) {
                        let hash = crypto.createHash('sha256').update(text).digest('hex');
                        pageQueue.push({
                            fUrl: url.fUrl,
                            url: url2,
                            text,
                            what: 'HTML',
                            status,
                            hash
                        });
                        linkQueue = linkQueue.concat(links);
                    }
            
                    if(!await Utils.urlsEqual(url2, url.fUrl) && !page) {
                        //frontier url is different than end url --> redirect
                        pageQueue.unshift({
                            fUrl: url.fUrl,
                            url: url.fUrl,
                            text: null,
                            what: 'REDIRECT',
                            status: 300
                        });
                        linkQueue.unshift({
                            from: url.fUrl,
                            to: url2,
                            redirect: true
                        });
                    }
                } else {
                    Logger.logWorker("Crawling not allowed for this site.", socket.id);
                    pageQueue.unshift({
                        fUrl: url.fUrl,
                        url: url.fUrl,
                        text: null,
                        what: 'BLACKLIST',
                        status: null,
                        hash: null
                    });
                }

                socket.emit('newPages', pageQueue, () => {
                    socket.emit('newLinks', linkQueue);
                });
                socket.emit('frontierInsert', frontierQueue);
                resolve();
            });
        });
    };

    const getPage = async(fUrl, page) => {
        let text;
        let url;
        let status;
        let database;
        let response;
        let downloaded = false;
    
        if(page) {
            text = page.html_content;
            url = page.url;
            status = page.http_status_code;
            database = page;
        } else {
            let d = await Downloader.downloadPage(fUrl);
            text = d.text;
            url = d.url;
            status = d.status;
            response = d.response;
            downloaded = true;
        }
        return {
            text,   
            url2: url,
            status,
            downloaded,
            response,
            database
        };
    }
};

run();
